﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Debug,F;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Libraries" Type="Folder">
			<Item Name="Plugins" Type="Folder">
				<Item Name="Button Cluster.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Button Cluster.lvlibp">
					<Item Name="Button Cluster.xctl" Type="XControl" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Button Cluster.lvlibp/Button Cluster.xctl"/>
					<Item Name="Version To Dotted String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Button Cluster.lvlibp/1abvi3w/vi.lib/_xctls/Version To Dotted String.vi"/>
					<Item Name="XControlSupport.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Button Cluster.lvlibp/1abvi3w/vi.lib/_xctls/XControlSupport.lvlib"/>
				</Item>
				<Item Name="Direct Control.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp">
					<Item Name="Public API" Type="Folder">
						<Item Name="Arguments" Type="Folder">
							<Item Name="Request" Type="Folder">
								<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Stop Argument--cluster.ctl"/>
								<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Show Panel Argument--cluster.ctl"/>
								<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Hide Panel Argument--cluster.ctl"/>
								<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Show Diagram Argument--cluster.ctl"/>
								<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Get Module Execution Status Argument--cluster.ctl"/>
								<Item Name="Set Sub Panel Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set Sub Panel Argument--cluster.ctl"/>
								<Item Name="Set Device Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set Device Argument--cluster.ctl"/>
								<Item Name="Set 37644 Page Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37644 Page Argument--cluster.ctl"/>
								<Item Name="Set 37645 Page Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37645 Page Argument--cluster.ctl"/>
								<Item Name="Set 37044 Page Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37044 Page Argument--cluster.ctl"/>
								<Item Name="Set 37045 Page Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37045 Page Argument--cluster.ctl"/>
								<Item Name="Write Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Write Argument--cluster.ctl"/>
								<Item Name="Write (Reply Payload)--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Write (Reply Payload)--cluster.ctl"/>
								<Item Name="Select Page Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Select Page Argument--cluster.ctl"/>
								<Item Name="Read Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Read Argument--cluster.ctl"/>
								<Item Name="Read (Reply Payload)--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Read (Reply Payload)--cluster.ctl"/>
							</Item>
							<Item Name="Broadcast" Type="Folder">
								<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Did Init Argument--cluster.ctl"/>
								<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Status Updated Argument--cluster.ctl"/>
								<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Error Reported Argument--cluster.ctl"/>
							</Item>
						</Item>
						<Item Name="Requests" Type="Folder">
							<Item Name="Show Panel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Show Panel.vi"/>
							<Item Name="Hide Panel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Hide Panel.vi"/>
							<Item Name="Stop Module.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Stop Module.vi"/>
							<Item Name="Show Diagram.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Show Diagram.vi"/>
							<Item Name="Set Sub Panel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set Sub Panel.vi"/>
							<Item Name="Set Device.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set Device.vi"/>
							<Item Name="Set 37644 Page.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37644 Page.vi"/>
							<Item Name="Set 37645 Page.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37645 Page.vi"/>
							<Item Name="Set 37044 Page.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37044 Page.vi"/>
							<Item Name="Set 37045 Page.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Set 37045 Page.vi"/>
							<Item Name="Write.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Write.vi"/>
							<Item Name="Select Page.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Select Page.vi"/>
							<Item Name="Read.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Read.vi"/>
						</Item>
						<Item Name="Start Module.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Start Module.vi"/>
						<Item Name="Synchronize Module Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Synchronize Module Events.vi"/>
						<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Obtain Broadcast Events for Registration.vi"/>
					</Item>
					<Item Name="Broadcasts" Type="Folder">
						<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Broadcast Events--cluster.ctl"/>
						<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Obtain Broadcast Events.vi"/>
						<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Destroy Broadcast Events.vi"/>
						<Item Name="Module Did Init.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Did Init.vi"/>
						<Item Name="Status Updated.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Status Updated.vi"/>
						<Item Name="Error Reported.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Error Reported.vi"/>
						<Item Name="Module Did Stop.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Did Stop.vi"/>
						<Item Name="Update Module Execution Status.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Update Module Execution Status.vi"/>
					</Item>
					<Item Name="Requests" Type="Folder">
						<Item Name="Request Events--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Request Events--cluster.ctl"/>
						<Item Name="Obtain Request Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Obtain Request Events.vi"/>
						<Item Name="Destroy Request Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Destroy Request Events.vi"/>
						<Item Name="Get Module Execution Status.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Get Module Execution Status.vi"/>
					</Item>
					<Item Name="Private" Type="Folder">
						<Item Name="SubVIs" Type="Folder">
							<Item Name="Insert VI to Sub Panel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Insert VI to Sub Panel.vi"/>
						</Item>
						<Item Name="Init Module.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Init Module.vi"/>
						<Item Name="Handle Exit.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Handle Exit.vi"/>
						<Item Name="Close Module.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Close Module.vi"/>
						<Item Name="Module Data--cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Data--cluster.ctl"/>
						<Item Name="Module Name--constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Name--constant.vi"/>
						<Item Name="Module Timeout--constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Timeout--constant.vi"/>
						<Item Name="Module Not Running--error.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Not Running--error.vi"/>
						<Item Name="Module Not Synced--error.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Not Synced--error.vi"/>
						<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Not Stopped--error.vi"/>
						<Item Name="Module Running as Singleton--error.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Running as Singleton--error.vi"/>
						<Item Name="Module Running as Cloneable--error.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Running as Cloneable--error.vi"/>
					</Item>
					<Item Name="Module Sync" Type="Folder">
						<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Destroy Sync Refnums.vi"/>
						<Item Name="Get Sync Refnums.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Get Sync Refnums.vi"/>
						<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Synchronize Caller Events.vi"/>
						<Item Name="Wait on Event Sync.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Wait on Event Sync.vi"/>
						<Item Name="Wait on Module Sync.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Wait on Module Sync.vi"/>
						<Item Name="Wait on Stop Sync.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Wait on Stop Sync.vi"/>
					</Item>
					<Item Name="Multiple Instances" Type="Folder">
						<Item Name="Module Ring" Type="Folder">
							<Item Name="Init Select Module Ring.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Init Select Module Ring.vi"/>
							<Item Name="Update Select Module Ring.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Update Select Module Ring.vi"/>
							<Item Name="Addressed to This Module.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Addressed to This Module.vi"/>
						</Item>
						<Item Name="Is Safe to Destroy Refnums.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Is Safe to Destroy Refnums.vi"/>
						<Item Name="Clone Registration.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Clone Registration/Clone Registration.lvlib"/>
						<Item Name="Test Clone Registration API.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Clone Registration/Test Clone Registration API.vi"/>
						<Item Name="Get Module Running State.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Get Module Running State.vi"/>
						<Item Name="Module Running State--enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Module Running State--enum.ctl"/>
					</Item>
					<Item Name="Tester" Type="Folder">
						<Item Name="Test Direct Control API.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Test Direct Control API.vi"/>
					</Item>
					<Item Name="Main.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Main.vi"/>
					<Item Name="Delacor_lib_QMH_Cloneable Module Admin.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Delacor/Delacor QMH/Libraries/Cloneable Module Admin_class/Delacor_lib_QMH_Cloneable Module Admin.lvclass"/>
					<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="GetHelpDir.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
					<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
					<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
					<Item Name="Rendezvous RefNum" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Rendezvous RefNum"/>
					<Item Name="Create New Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
					<Item Name="Not A Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
					<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
					<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
					<Item Name="Create Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Create Rendezvous.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Release Waiting Procs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
					<Item Name="Wait at Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
					<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
					<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
					<Item Name="Destroy Rendezvous.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
					<Item Name="usereventprio.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/event_ctls.llb/usereventprio.ctl"/>
					<Item Name="BuildHelpPath.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
					<Item Name="Get Text Rect.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
					<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
					<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
					<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
					<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
					<Item Name="Three Button Dialog.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
					<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
					<Item Name="Not Found Dialog.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
					<Item Name="Set Bold Text.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
					<Item Name="eventvkey.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
					<Item Name="TagReturnType.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
					<Item Name="ErrWarn.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
					<Item Name="Details Display Dialog.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
					<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
					<Item Name="Find Tag.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
					<Item Name="Format Message String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="Error Code Database.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
					<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
					<Item Name="Set String Value.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
					<Item Name="Check Special Tags.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
					<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
					<Item Name="DialogType.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
					<Item Name="General Error Handler.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
					<Item Name="Simple Error Handler.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Simple Error Handler.vi"/>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Message Queue Logger.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/Message Queue Logger.vi"/>
					<Item Name="LVPointTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Direct Control.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				</Item>
				<Item Name="Memory Table Plugin.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp">
					<Item Name="Public" Type="Folder">
						<Item Name="Initialize Memory Table Plugin.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/Public/Initialize Memory Table Plugin.vi"/>
					</Item>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Create Register Data File.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/SubVIs/Create Register Data File.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="Main.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/Main.vi"/>
					<Item Name="Message Queue.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/menus/Categories/User Library/Library/Message Queue/Message Queue.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Read Register INI File.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/SubVIs/Read Register INI File.vi"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="subFile Dialog.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="UI Reference.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp">
					<Item Name="Controls" Type="Folder">
						<Item Name="UI - All UI Objects Refs Memory - Operation.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/Controls/UI - All UI Objects Refs Memory - Operation.ctl"/>
					</Item>
					<Item Name="SubVIs" Type="Folder">
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Array.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Array.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Boolean.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Boolean.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Cluster.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Cluster.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Digital.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Digital.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Enum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Enum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Knob.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Knob.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ListBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ListBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Numeric.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Numeric.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Path.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Picture.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Picture.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - RefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Ring.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Ring.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Slide.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Slide.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - String.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TabControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TabControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Table.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Table.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - String version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - String version.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Typedef version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Typedef version.vi"/>
					</Item>
					<Item Name="Init Buttons.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/Init Buttons.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory - Get Reference.vi"/>
					<Item Name="UI - All UI Objects Refs Memory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory.vi"/>
				</Item>
			</Item>
			<Item Name="USB Communication" Type="Folder">
				<Item Name="USB-I2C" Type="Folder">
					<Property Name="NI.SortType" Type="Int">3</Property>
					<Item Name="USB-I2C.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp">
						<Item Name="Palette" Type="Folder">
							<Item Name="USB-I2C.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C.mnu"/>
							<Item Name="USB-I2C Example.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Example.mnu"/>
						</Item>
						<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
						<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
						<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
						<Item Name="Get File Extension.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
						<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
						<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
						<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
						<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
						<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
						<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
						<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
						<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
						<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
						<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
						<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
						<Item Name="subTimeDelay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
						<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
						<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
						<Item Name="USB-I2C Create.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Create/USB-I2C Create.lvclass"/>
						<Item Name="USB-I2C.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C/USB-I2C.lvclass"/>
						<Item Name="VI Tree.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/VI Tree.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
					<Item Name="CP2112-HID.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp">
						<Item Name="Class" Type="Folder">
							<Item Name="CP2112-HID.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/CP2112-HID.lvclass"/>
						</Item>
						<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
						<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
						<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
						<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
						<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
						<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
						<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
						<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
						<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
						<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
						<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
						<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
						<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
						<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
						<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
						<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
						<Item Name="Silicon Labs CP2112.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/APIs/Library/Silicon Labs CP2112.lvlib"/>
						<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
						<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
						<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
						<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/CP2112-HID.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="Message Queue Logger.vi" Type="VI" URL="../I2C Script Editor/Message Queue Logger.vi"/>
		</Item>
		<Item Name="Support Library" Type="Folder">
			<Item Name="Type Sensitive Popup.lvlib" Type="Library" URL="../../../Libraries - Code/Type Sensitive Popup/Type Sensitive Popup.lvlib"/>
		</Item>
		<Item Name="I2C Script Editor.lvlib" Type="Library" URL="../I2C Script Editor/I2C Script Editor.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
				<Item Name="AES Algorithm.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Algorithm.vi"/>
				<Item Name="AES Keygenerator.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Keygenerator.vi"/>
				<Item Name="AES poly mult.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES poly mult.vi"/>
				<Item Name="AES Rounds .vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Rounds .vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array__ogtk.vi"/>
				<Item Name="Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path__ogtk.vi"/>
				<Item Name="ClassID Names Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/ClassID Names Enum__ogtk.ctl"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Create Dir if Non-Existant__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Create Dir if Non-Existant__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
				<Item Name="Extract Window Names.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Extract Window Names.vi"/>
				<Item Name="Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array__ogtk.vi"/>
				<Item Name="Get Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element by Name__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get LVWUtil32.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/SubVIs/Get LVWUtil32.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Task List.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Get Task List.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Window RefNum.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Get Window RefNum.vi"/>
				<Item Name="List Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory Recursive__ogtk.vi"/>
				<Item Name="List Directory__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory__ogtk.vi"/>
				<Item Name="Make Window Always on Top.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Make Window Always on Top.vi"/>
				<Item Name="Maximize Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Maximize Window.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Defer Panel Updates.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Defer Panel Updates.vi"/>
				<Item Name="MGI Get VI Control Ref[].vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Get VI Control Ref[].vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Origin at Top Left.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Origin at Top Left.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
				<Item Name="Not a Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Not a Window Refnum"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="PostMessage Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/PostMessage Master.vi"/>
				<Item Name="Quit Application.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Quit Application.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Set Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Cluster Element by Name__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Window Z-Position.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Set Window Z-Position.vi"/>
				<Item Name="Show Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Show Window.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Window Refnum"/>
				<Item Name="WinUtil Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/WinUtil Master.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="_ExponentialCalculation__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_ExponentialCalculation__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(I32)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(I32)__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(String)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(String)__lava_lib_ui_tools.vi"/>
				<Item Name="_fadetype__lava_lib_ui_tools.ctl" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_fadetype__lava_lib_ui_tools.ctl"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="CenterRectInBnds.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/CenterRectInBnds.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="compatReturnToEnter.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReturnToEnter.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Delacor_lib_QMH_Cloneable Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Cloneable Module Admin_class/Delacor_lib_QMH_Cloneable Module Admin.lvclass"/>
				<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
				<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Fade (Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade (Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="Fade__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade__lava_lib_ui_tools.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GPMath.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Math/GPMath.lvlib"/>
				<Item Name="GPNumeric.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Numeric/GPNumeric.lvlib"/>
				<Item Name="GPString.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/String/GPString.lvlib"/>
				<Item Name="GPTiming.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Timing/GPTiming.lvlib"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="POffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/POffsetRect.vi"/>
				<Item Name="PointInRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/PointInRect.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="RectAndRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectAndRect.vi"/>
				<Item Name="Rectangle Size__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Alignment/Rectangle Size__lava_lib_ui_tools.vi"/>
				<Item Name="RectCentroid.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectCentroid.vi"/>
				<Item Name="RectSize.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectSize.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SetTransparency(Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency(U8)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(U8)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency__lava_lib_ui_tools.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Timer.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/National Instruments/Simple DVR Timer API/Timer.lvclass"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Find Search Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Find Search Mode.ctl"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Authorization.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Authorization.lvlib"/>
			<Item Name="Control Refnum.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
			<Item Name="Encrypt &amp; Decrypt String.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Encrypt &amp; Decrypt String.vi"/>
			<Item Name="Get BIOS Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get BIOS Info.vi"/>
			<Item Name="Get Key.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Key.vi"/>
			<Item Name="Get Volume Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Volume Info.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
			<Item Name="Mode-Enum.ctl" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Control/Mode-Enum.ctl"/>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System Exec.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/System Exec/System Exec.lvlib"/>
			<Item Name="System.Management" Type="Document" URL="System.Management">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="VI.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/VI Refnum/VI.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="I2C Script Editor" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{28B6B447-272B-4FFE-B23B-6662E0A951DC}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">V1.8.1-20230809
1.Fix Multi Port Select issue

V1.8.0-20230515
1.Add Support Multi Port control

V1.7.0-20221018
1.Add button for stop process when fail happen

V1.6.0-20221012
1.Add Get GPIO function

V1.5.0-20220902
1.Add GPIO Control for CP2112

V1.4.3-20220215
1.Log Read Data =&gt;Data

V1.4.2-20220214
1.Add Elapsed Time

V1.4.1-20210723
1.Display Bug Fixed

V1.4.0-20210707
1.Now Support For DQMH Call Run Script Test

V1.3.0-20210422
1.Add Bit Rate for setting

V1.2.0-20201007
1.Add I2C Delay Read and Write

V1.1.1-20200925
1.Reg Fix 16 bit bug

V1.1.0-20200828
1.Support Bit16 Register Address

V1.0.2-20200819
1.Script Path Add Change Start Path after Value Changed

V1.0.1-20200814
1.PASS and FAIL String move to first line
2.Script Log Add auto move to last status

V1.0.0</Property>
				<Property Name="Bld_buildSpecName" Type="Str">I2C Script Editor</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B6541439-E732-428E-958F-8F4719E88934}</Property>
				<Property Name="Bld_version.build" Type="Int">23</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">8</Property>
				<Property Name="Bld_version.patch" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">I2C Script Editor.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{66B8845D-F207-4F50-9815-32A6DDFFDA7E}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/I2C Script Editor.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Support Library</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Luxshare-Tech</Property>
				<Property Name="TgtF_fileDescription" Type="Str">I2C Script Editor Module</Property>
				<Property Name="TgtF_internalName" Type="Str">I2C Script Editor</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright (c) 2020-2023 Luxshare-Tech Corporation. All rights reserved</Property>
				<Property Name="TgtF_productName" Type="Str">I2C Script Editor</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F80FBC05-C831-4666-97F2-75D7D03C7B7F}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">I2C Script Editor.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
